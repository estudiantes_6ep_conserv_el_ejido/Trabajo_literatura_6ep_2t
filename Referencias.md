- Fernández, J. D. et Vico, Francisco. [AI Methods in Algorithmic composition: a comprehensive survey](
  https://arxiv.org/pdf/1402.0585), Journal of Artificial Intelligence research, nº 48. 2013
- Dredge, Sutart, [AI and music: will be slaves to the algorithm?](https://www.theguardian.com/technology/2017/aug/06/artificial-intelligence-and-will-we-be-slaves-to-the-algorithm), agosto, 6, 2017.
- Fidalgo, Paloma, [Björk compone música asistida por IA ara un hotel de Nueva York](
https://www.elplural.com/leequid/voces/bjork-compone-musica-asistida-inteligencia-artificial-hotel-nueva-york_232140102), El Plural, enero, 29, 2020.
- Tec Review, [La IA escribe notas y compone música](https://tecreview.tec.mx/la-inteligencia-artificial-escribe-notas-compone-musica/), marzo, 12, 2019.
- Sánchez, Rosalía, [La inteligencia artificial compone la décima sinfonía de Beethoven](
  https://www.abc.es/cultura/musica/abci-inteligencia-artificial-compone-decima-sinfonia-beethoven-201912140057_noticia.html), diciembre, 16, 2019.
- Juárez, Belén, [La inteligencia artificial cambia la manera de crear y escuchar música](https://elpais.com/tecnologia/2019/12/11/actualidad/1576076187_002705.html), diario El Pais, diciembre, 15, 2019.
- [La inteligencia artificial compone música pop](https://elpais.com/tecnologia/2016/09/21/actualidad/1474454261_634391.html?rel=mas), diario El Pais, septiembre, 22, 2016.
- [Una inteligencia artificial crea música al estilo de Bach](
  https://www.abc.es/tecnologia/abci-inteligencia-artificial-crea-musica-5719087690001-20180125090400_video.html), diario ABC, enero, 25, 2018.
- Álvarez, Raul, [La inteligencia artificial ya es capaz de componer música y estas canciones lo demuestran] (https://www.xataka.com/robotica-e-ia/la-inteligencia-artificial-ya-es-capaz-de-componer-musica-y-estas-canciones-lo-demuestran), Xataka, diario digital, septiembre, 23, 2016.
- Manuel, [Esta web genera música hecha con IA con solo pulsar un botón](
 https://www.elespanol.com/omicrono/software/20190429/genera-musica-hecha-ia-solo-pulsar-boton/394711757_0.html),
 abril, 29, 2019.
- Ospina Deaza, Juan Camilo, [Chao humanos: la inteligencia artificial ya compone y escoge nuestra música](
  https://www.shock.co/musica/chao-humanos-la-inteligencia-artificial-ya-compone-y-escoge-nuestra-musica-ie42),
  noviembre, 4, 2019.
- Varios, Wikipedia [Composición algorítmica](https://es.wikipedia.org/wiki/Composici%C3%B3n_algor%C3%ADtmica),
    consultada en marzo, 18, 2020
- Varios, Wikipedia [Music and artificial intelligence](
  https://en.wikipedia.org/wiki/Music_and_artificial_intelligence), consultada en marzo, 18, 2020
- Varios, Wikipedia [Computational creativity](
  https://en.wikipedia.org/wiki/Computational_creativity#Musical_creativity), consultada en marzo, 18, 2020
- Maurer IV, John A. [The history of algorithmic composition](
  https://ccrma.stanford.edu/~blackrse/algorithm.html), marzo, 1999.

## Introducción

El uso efectivo de la música en los videojuegos puede ir desde el sonido de una 
señal al final de una escena o un nivel, hasta llevarnos a un clímax emocional 
que te deje con ganas de más.

De hecho, el uso de la música en los juegos es notablemente similar a la forma
en que se utiliza la música en el mundo del cine, y cada vez se reconoce más la
importancia de las bandas sonoras de los juegos y el talento que se necesita
para hacerlos funcionar.

## La música en los videojuegos

La música en esta industria es reconocida, prueba de ello fue la concesión en 
2014 de un BAFTA a James Bonney y Garry Schyman por BioShock Infinite lanzado en
2013, lo que demuestra lo lejos que ha llegado la música para juegos desde el 
comienzo de las bandas sonoras en formato 8 bits y MIDI.

Esta evolución es obvia para cualquier usuario que haya jugado con una consola 
de última generación como la PlayStation 4. La PS 4 cuenta con tecnología Dolby,
que reproduce el sonido envolvente en cada título.

## Los comienzos en los años 70 y 80

Los videojuegos tal y como los conocemos empezaron a aparecer a finales de la 
década de los 70 del pasado siglo, con juegos de arcade y versiones para 
consolas de juegos muy populares, resultando todo un éxito.
La música era creada a través de simples chips sintéticos, generando sonidos en
un estilo que se conoció como el subgénero “chiptune”. Esto fue un paso más allá
del completo silencio o de los pitidos y booms básicos de los primeros juegos y
que más tarde se hicieron famosos en el juego Pong de la marca japonesa Atari.

A medida que avanzábamos en la década de los 80, la música de los videojuegos 
comenzó a desarrollarse tan rápido como la propia tecnología. Las bandas sonoras
eran cada vez más dinámicas y empezaron a convertirse en algo normal en cada 
título. Se utilizaba la música para comunicar directamente la información al 
jugador.

El famoso juego de 1987, R.B.I. Baseball, se convirtió en un gran ejemplo de la
música que se usaba para reflejar las acciones del jugador.

[R.B.I. Baseball](
https://www.youtube.com/watch?time_continue=22&v=5SK4MiHFDBQ&feature=emb_logo)

## Música electrónica y digital en los años 90

A medida que los ordenadores como el Commodore Amiga y consolas como la Sega 
Mega Drive empezaron a ofrecer procesadores de audio más avanzados, los 
compositores de música para juegos fueron más ambiciosos.

El compositor Yuzo Koshiro, que actuó por primera vez en España el año pasado 
en el festival de Sónar 2018 en Barcelona, utilizó el hardware de Mega Drive 
para crear bandas sonoras pegadizas y de estilo tecno para la serie de títulos 
como The Revenge of Shinobi y Streets of Rage, introduciendo líneas de bajo 
eléctrico y sonidos electrónicos “trancey”, que se hicieron populares en los
90.

El uso repetido de ciertas frases era necesario debido a la falta de memoria en
las consolas, lo que llevó a la creación de clásicos adictivos como la banda 
sonora de Super Mario Bros de Koji Kondo y que se puede escuchar en el siguiente
vídeo.

[Banda sonora de Super Mario Bros, Koji Kondo](
https://www.youtube.com/watch?time_continue=9&v=MBQTs7ZtKsQ&feature=emb_logo)

## Los estados de ánimo a partir del nuevo siglo

A medida que entramos en la década del 2000 con la música electrónica ya 
avanzada, la introducción del software Dolby Digital en las consolas transformó
la profundidad y la complejidad de las bandas sonoras de los juegos, con músicos
como Trent Reznor componiendo música para videojuegos como Doom y que hace unos
meses buscaba miembros para un coro.

Los juegos complejos como Halo fueron aclamados por sus bandas sonoras, que 
aportan un toque conmovedor y emocional a los juegos y aumentan la calidad de
inmersión de un juego. Con las consolas de última generación diseñadas ahora
como sistemas de entretenimiento completos, dispositivos construidos para ver
películas, las bandas sonoras de los juegos pueden ahora tener la misma calidad
que las bandas sonoras cinematográficas.

[B.S.O. de Halo](
https://www.youtube.com/watch?v=xufR_zt9o10)

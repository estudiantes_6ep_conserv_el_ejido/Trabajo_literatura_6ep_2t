## Utilización de la electrónica y música sintetizada en músicas populares

HIP HOP, POP, ROCK, MÚSICA ELECTRÓNICA...

### Evolución de la música popular

A principios de los años 60 empezó a disminuir la presencia en las canciones 
populares de los acordes de séptima dominante, presentes en el jazz y el blues.

Y, a partir de 1964, tuvo lugar una «invasión» de grupos británicos en el 
panorama internacional. Lo que se escuchaba en esa época eran bandas como los
Rolling Stones que introdujeron un sonido rockero radicalmente nuevo y que,
posteriormente, irian incorporando sonidos nuevos al rock tales como música
electrónica.

[Rolling Stones](https://www.youtube.com/watch?v=O4irXQhgMqg)

En 1983, se puso de moda las nuevas tecnologías, los sintetizadores, los 
samplers y las cajas de ritmos. Los artístas más escuchados eran Michael 
Jackson, The police y Eurythmics.

[Eurythmics](https://youtu.be/qeMFqkcPYcg)

Curiosamente el tecnopop que tanto caracterizó a los años 80, convivió a veces
con el soul y el estilo afroamericano doo-wop, de Billy Joel, mientras sonaban
temas como «all night long» de Lionel Richie, que incorpora influencias 
caribeñas al funk.

[Lionel Ritchie](https://youtu.be/nqAvFx3NxUM)

## Aparición del rap, hip-hop y géneros similares

Ya a partir del año 1991, comenzó la llegada a la música comercial del rap, 
el hip hop y otros géneros relacionados. Ejemplos típicos de 1991, la música 
del rapero Busta Rhymes, Nas, y Snoop Dogg.

Triunfaban temas de rap como «Around the Way Girl», de LL Cool J.

[LL Cool J.](https://youtu.be/Fwgt9Dxyyv0)

Fue una gran revolución,ya que se podía tener una canción pop... sin armonía,
gracias a los sintetizadores, cajas de ritmos, etc.
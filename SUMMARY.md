# Summary

* [Presentación](README.md)
* [Música en los videojuegos](Musica_en_los_videojuegos.md)
* [Musica compuesta por ordenador](Musica_compuesta_por_ordenador.md)
* [Grupos virtuales](Grupos_virtuales.md)
* [Música electrónica y sintetizada en géneros populares](Electronica_y_musica_sintetizada.md)
* [Referencias utilizadas](Referencias.md)

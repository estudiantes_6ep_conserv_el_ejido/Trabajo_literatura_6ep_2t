## Definición de grupo virtual

Es un grupo de música en el que sus miembros no son corpóreos debido a que
son personajes animados. La voz y los hologramas han ido variando a lo largo
de los años con el desarrollo de las nuevas tecnologías.

## Historia de los grupos virtuales

El primer grupo conocido fue Alvin and the Chipmunks creado en 1958 por Ross
Bagdasarian, que decidió modificar su voz y acelerarla. Como resultado obtuvo
una voz aguda y chillona que se parecía bastante a una ardilla. Daban
conciertos y asistían a programas como marionetas hasta que, poco a poco,
conforme adquirieron más popularidad hicieron películas protagonizadas por
ellos e incluso ganaron varios premios Grammys.

[Primera canción de Alvin y las ardillas](https://youtu.be/wdN_8OlJGs0)

Tras esto empezaron a crearse bandas virtuales a partir de series de televisión.
Destaca por el ejemplo Josie and the Pussycats de Archie en los 70, y en los 80
Dr. Teeth and The Electric Mayhem que formaban parte de los Muppets.

[Josie and the Pussycats](https://youtu.be/EEt3dKelbaU)

[Dr. Teeth and The Electric Mayhem cover de Bohemian Rhapsody](
https://youtu.be/tgbNymZ7vqY)

<!-- (aquí salen más personajes no solo los de la banda pero me 
     parecía interesante ponerlo) !-->

## Artistas virtuales en el SXXI

A partir del 2000 el termino se popularizo con la aparición de la banda británica
Gorillaz en 1998, creada por Damon Albarn y Jamie Hewlett. La banda
consta de cuatro miembros ficticios: 2-D, Noodle, Murdoc Niccals y Russel
Hobbs. La mayoría de sus canciones vienen acompañadas de vídeos
musicales animados, tanto en 2D y 3D, que suelen poner en sus conciertos

[Gorillaz](https://youtu.be/1V_xRb0x9aw)

También encontramos a Miku hatsune, la artista más destacada perteneciente
a Vocaloid, una aplicación software de síntesis de voz. Fue creado por Yamaha
corporation en 2004 basándose en trabajos del *Music Technology Group* de la
Universidad Pompeu i Fabra de Barcelona, aunque en su primer lanzamiento no 
encontramos a Miku Hatsune sino a otros vocaloid que actualmente no están 
disponibles. Mediante este programa las personas tiene la capacidad de crear 
canciones (con su respectiva letra, melodía…) y modificarlas a su gusto. 
Además también es posible crear animaciones en 3D de manera que haga una 
coreografía de la canción y se pueda reproducir su holograma. De esta 
manera, tanto Hatsune Miku como otros vocaloids han dado numerosos 
conciertos.

[Video concierto Miku Hatsune](https://youtu.be/YSyWtESoeOc)

Otras bandas a destacar son Dethklock, Genki rockets y K/DA

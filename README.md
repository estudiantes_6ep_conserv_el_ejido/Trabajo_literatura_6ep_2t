![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

Trabajo de literatura y audición musical (2T): Electrónica y computación en la 
música

---

## Estructura del trabajo

1. Música en los videojuegos: historia y evolución desde los 8 bits
2. Música creada por ordenadores
3. Grupos virtuales (Gorillaz, Miku Hatsune, otros.)
4. Utilización de la electrónica y música sintetizada en músicas populares: 
   hip-hop, rock, música electrónica y otros géneros

## Presentación del trabajo

Con la aparición de la electrónica y los computadores en los tiempos modernos,
muchos sectores de actividad han cambiado, desde la industria hasta los
hogares.

La música no ha podido mantenerse ajena y ya experimentadores e innovadores como
Oliver Mesiaen, John Cage y otros han introducido esta tecnología en la música.

A lo largo de este trabajo vamos a repasar otras áreas donde la música se
conjuga con la electrónica y la computación: desde los videojuegos a los grupos
virtuales, pasando por música compuesta directamente por las máquinas (con
o sin ayuda de humanos) y como también se ha introducido en los géneros
populares.

[Presentación en formato LibreOffice](Presentación.odp)


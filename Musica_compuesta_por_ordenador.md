## Musica compuesta por ordenador

En la relación de la electrónica y la computación con la música hay un aspecto
que en los últimos tiempos ha venido investigándose: la composición de música
por las propias máquinas.

El avance de la inteligencia artificial (IA) está, en los últimos años,
produciendo unos resultados ya bastante realistas en cuanto a lo que podemos
entender como música.

En esta sección vamos a dar un repaso al estado del arte musical producido por
máquinas.

## Definiciones

### Composición algorítmica

Los algoritmos, el nombre que recibe un conjunto de instrucciones o reglas
bien definidas -es decir no ambiguas- para solucionar un problema, realizar un
cálculo u otras tareas; se utilizan desde hace siglos para la composición de
música, como por ejemplo son los procedimientos para la creación de
contrapunto. Actualmente, este uso de algoritmos se utiliza con computadores
y pueden emplear distintos modelos para la creación de música:

- Modelos matemáticos: uso de ecuaciones y eventos al azar
- Sistemas basados en el conocimiento: uso del código estético de un determinado
  género
- Gramática: uso de la gramática musical
- Método evolucionario: uso de algoritmos genéticos
- Sistemas de auto-aprendizaje: alimentados por datos, la máquina va 
  aprendiendo. Utilizan redes neurales.
- Sistemas híbridos: uso de distintos algoritmos.

Una definición más formal podría ser esta: «el proceso de usar algunos procesos 
formales para crear música con la minima intervención humana» (Alpern, 1995)

[Excentric japanese, para chelo](https://web.archive.org/web/20160216162852/http://www.randomusic.com/samples/Excentric%20Japanese%20%28cello-111-2-mp3%29.wav)

### Música compuesta por inteligencia artificial

El avance de la digitalización de la información ha permitido crear grandes
redes de datos e información que, por regla general, se utiliza para consumo
humano; sin embargo, esta masa de datos tiene una función vital en uno de los
avances informáticos sobre los que se trabaja, las IIAA. Mediante técnicas
como el aprendizaje automática y las redes neuranales las máquinas, alimentadas
por datos casi infinitos están aprendiendo a componer música y otras artes.

Es este el caso de dos canciones nacidas de los trabajos de FlowMachines,
proyecto del *laboratorio de investigación de Sony*: *Daddy's Car*, siguiendo 
el estilo de los Beatles y *The ballad of Mr Shadow*, inspirada por 
compositores como Irving Berling, Duke Ellington, George Gershwin y Cole 
Porter; y que se dieron a conocer en 2016.

[Daddy's Car](https://www.youtube.com/watch?v=LSHZ_b05W7o)

[Mr Shadow: a song composed by AI](https://www.youtube.com/watch?v=lcGYEXJqun8)

También se utiliza la IA para crear música en el estilo de compositores
antigüos, como el caso de *DeepBach*, también por parte de Sony y dirigido el
proyecto por François Pachet:

[DeepBach: harmonization in the style of Bach generated using deep learning](
https://www.youtube.com/watch?v=QiBM7-5hA6o)

## Breve historia

El uso de algoritmos en la música se remonta a la antiguedad, pues ya se puede
encontrar en Pitágoras y en los trabajos de Ptolomeo y Platón.

Posteriormente el mismo Mozart ya usó técnicas de composición automática en su 
*Musikalisches Wurfelspiel*; y ya, en la era moderna, John Cage utilizó la 
aleatoridad para componer; y Olivier Messiaen en su estudio de piano *Mode de 
valeurs et d'inensites* utilizó patrones de notas, ritmos, dinámicas...

Con la llegado de los computadores, el intento de usar algoritmos se aceleró y
encontramos a dos pioneros: Lejaren Hiller y Iannis Xenakis

Lejaren Hille y leonard Isaacson, en 1955 y 56, utilizando el ordenador digital 
de alta velocidad (para la época) Illiac obtuvieron la Illiac Suite (1958). Sus
trabajos fueron continuados y mejorados posteriormente en la decada de los 60.

[Lejaren Hiller, Illiac suite](https://www.youtube.com/watch?v=n0njBFLQSk8)

Iannis Xenakis creó un programa que producía datos de partida para sus 
composiciones estocásticas (esto es aleatorias). El proceso se detallo en 
*Formalized music* de 1963 y Xenakis produjo Atrées en 1962 y Morisma-Amorsima 
en el mismo año.

[Iannis Xenakis, Metastasis](https://www.youtube.com/watch?v=SZazYFchLRI)

En 1960, el investigador ruso R. Kh. Zaripov, publica uno de los primeros 
artículos científicos sobre composición algorítimica utilizando un ordenador 
«Ural-1».

Otro ejemplo temprano fue Olson en 1961, con un ordenador dedicado capaz de
componer melodias nuevas en base a otras utilizando unos métodos estadisticos
llamados procesos de Markov.

Ya en 1965, Ray Kurzweil presentó una pieza de piano creada por ordenador a 
partir del reconocimiento de patrones de varias composiciones.

En los años 80 del siglo XX, encontramos los trabajos de David Cope y su 
aplicación *EMI* («experiments in Musical Intelligence») ya basados en IA.

Y a finales de siglo, en 1994, se generan 11.000 fragmentos musicales mediante
una red neuronal; y en 1996 se produce un álbum completo.

Entrado el SXXI, en 2010, encontramos a Iamus, un supercomputador de la U. de 
Málaga con Debian Gnu/Linux como s.o., el primer ordenador que es capaz de 
producir composiciones que los interpretes profesionales pueden tocar. La
tecnología que había tras Iamus se llamaba Melomics y en 2012 lanza un primer 
álbum grabado por la London Symphony orchestra.

En otros campos podemos encontrar improvisaciones en Jazz por el robot Shimon,
desarrollado por Gil Weinber de Georgia Tech.

Actualmente existen multitud de empresas emergentes, apadrinadas a veces por
gigantes de la música como los estudios Abbey Road de Londres, o Jukedeck de la 
misma ciudad, Humtap en San Francisco, Groov.AI de Mountain View o los mencionados
FlowMachines de Sony.

## Aplicaciones que componen música

- [Listado extenso en Wikipedia](
  https://es.wikipedia.org/wiki/Composici%C3%B3n_algor%C3%ADtmica#Software)

- Amper, [una IA de producción musical](https://www.youtube.com/watch?v=GIpI0Fus70A)

- [Musenet](https://openai.com/blog/musenet/), movida por OpenAI, en esta web
puedes generar canciones con hasta 10 instrumentos distintos y 15 estilos 
diferentes.

## Músicos y computadores: el futuro

Si hay quien ve en esta música compuesta por máquinas un futuro incierto, hay
quien ve una oportunidad, un nuevo campo donde los músicos pueden cooperar con
las creaciones artificiales para crear nuevos y excitantes tipos de música.

Encontramos opiniones encontradas en este aspecto como pueden ser las de
Newton-Rex de Jukedec, afirmando que llegará un punto en que la música creada 
por máquinas será indistinguible de la creada por humanos; o la de Drew
Silverstein y Mark Mulligan que creen que la expresión de las emociones y
sentimientos es algo que las máquinas tienen un largo camino por delante para
poder replicar.

Pero este campo es más sutil que la simple polaridad blanco-negro, ya a día de
hoy los músicos están componiendo música apoyandose en el uso de IA, como por
ejemplo la islandesa Björk, Julianna Barwick o Taryn Souther que se ha apoyado
en la IA para la producción de un disco completo.

Björk en la composición *Kórsafan* utiliza la IA para los arreglos corales:

[Learn how Björk worked with AI on her new composition](
https://www.youtube.com/watch?v=HXSSA064EEs)

[Break Free, de Taryn Southern](https://www.youtube.com/watch?v=XUs6CznN8pw)

No obstante también hay otros campos donde las creaciones musicales generados 
por IA pueden ser realmente útiles: aquellas creaciones artísticas que 
necesiten música pero, por motivos presupuestarios, no pueden permitirse la 
contratación de un compositor profesional; estamos hablando de directores de 
cortometrajes o los nuevos profesionales del reportaje de las plataformas de 
difusión de vídeos como Youtube o similares.

Y no solo eso, también se puede utilizar para que las IA puedan acompañar a
estudiantes de música, como por ejemplo hace *Alice*, una aplicación que está
aprendiendo a tocar el piano como si fuera un niño y que, a partir de las
primeras notas que toca el interprete humano, es capaz de acompañarlo a su
mismo nivel.

Se suman otras posibilidades como la existencia de prótesis que han logrado
que músicos que han perdido extremidades puedan volver a interpretar música.
El *Music Technology* del Georgia Tech Center (GTCMT) ha permitido a un
bateria volver a tocar mediante un brazo robótico que utiliza la IA para
controlar el miembro.

Pero no se queda aquí, otros proyectos como Vochlea o Popgun, lo que permiten
es una especie de reconocimiento verbal musical, transformando sonidos emitidos
por un humano en golpes de bateria, trompetas y otros y ayudando a transformar
de forma rápida fugaces ideas musicales mentales en música real.

Un último campo donde la IA muestra sus posibilidades es la finalización de
composiciones incompletas de compositores ya desaparecidos. Es el caso de la
décima sinfonía de Beethoven, que se ha puesto en manos de una IA para su
finalización con motivo del 250 aniversario del nacimiento del músico de Bonn
y que se estrenará interpretado por la orquesta Beethoven de Boon en abril de
2020. No esta exenta de críticas la iniciativa, por supuesto, como por ejemplo
la de Barry Coopper («es muy aburrido y no suena para nada a Beethoven [...]»)
o la del crítico musical Manuel Brug («ninguna computadora podrá nunca adivinar
lo que pensó Beethoven»).

El futuro está aquí en este aspecto y en otros y traerá sus propios problemas,
como, por ejemplo, sobre quien recaen los derechos de autoria de las 
composiciones de las IIAA, y con que tipo de licencia.

Y, para terminar, otro aspecto donde los ordenadores a día de hoy ya están 
interactuando con los músicos es encima del escenario, donde desde hace años
as IIAA se utilizan para el manejo de luces y efectos.
